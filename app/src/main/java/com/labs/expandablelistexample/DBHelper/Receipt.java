package com.labs.expandablelistexample.DBHelper;

public class Receipt {

    int id;
    String _year;
    String _month;

    public  Receipt(){

    }

    public Receipt(  String _year, String _month) {

        this._year = _year;
        this._month = _month;
    }
    public Receipt( int id, String _year, String _month) {
        this.id= id;
        this._year = _year;
        this._month = _month;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String get_year() {
        return _year;
    }

    public void set_year(String _year) {
        this._year = _year;
    }

    public String get_month() {
        return _month;
    }

    public void set_month(String _month) {
        this._month = _month;
    }


}
