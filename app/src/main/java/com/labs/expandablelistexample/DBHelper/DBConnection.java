package com.labs.expandablelistexample.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBConnection extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "receiptManager.db";
    private static final String TABLE_RECEIPTS = "receipts";
    private static final String KEY_ID = "id";
    private static final String KEY_YEAR = "year";
    private static final String KEY_MONTH = "month";

    public DBConnection(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_RECEIPTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_YEAR  + " TEXT,"
                + KEY_MONTH  + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIPTS);

        // Create tables again
        onCreate(db);
    }

    // code to add the new receipt
    public void addReceipt(Receipt receipt) {
//        System.out.println(receipt+"Rec");
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_YEAR, receipt.get_year());
        values.put(KEY_MONTH, receipt.get_month());

        // Inserting Row
        db.insert(TABLE_RECEIPTS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }


    // code to get the single contact
   public Receipt getReceipt(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_RECEIPTS, new String[] { KEY_ID,
                        KEY_YEAR, KEY_MONTH }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Receipt receipt = new Receipt(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return receipt;
    }

    public List<Receipt> getAllReceipts() {
        List<Receipt> contactList = new ArrayList<Receipt>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_RECEIPTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Receipt receipt = new Receipt();
             //   receipt.setID(Integer.parseInt(cursor.getString(0)));
                receipt.set_year(cursor.getString(1));
                receipt.set_month(cursor.getString(2));
                // Adding contact to list
                contactList.add(receipt);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    public List<String> getYear()
    {
        List<String> yearList = new ArrayList<String>();
//                        yearList.add("sample");

        String yearQuery = "SELECT DISTINCT(year) FROM " + TABLE_RECEIPTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(yearQuery, null);
        if (cursor.moveToFirst()) {
            do {
              yearList.add(cursor.getString(cursor.getColumnIndex(DBConnection.KEY_YEAR)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return  yearList;
    }


    public  List<String> getMonth(String year)
    {
        System.out.println(year+"Year in db");
        List<String> monthList = new ArrayList<String>();
        //String condition = KEY_YEAR+" = "+ year;
        String monthQuery = "SELECT DISTINCT(month) FROM " + TABLE_RECEIPTS + " WHERE "+ KEY_YEAR + "="+ year;
        System.out.println(monthQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(monthQuery, null);
        if (cursor.moveToFirst()) {
            do {
                monthList.add(cursor.getString(cursor.getColumnIndex(DBConnection.KEY_MONTH)));
            } while (cursor.moveToNext());
        }

        return monthList;
    }


}
