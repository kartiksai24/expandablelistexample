package com.labs.expandablelistexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
//import android.content.DialogInterface;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.labs.expandablelistexample.DBHelper.DBConnection;
import com.labs.expandablelistexample.DBHelper.Receipt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    //ExpandableListDataPump expandableListDataPump;
    DBConnection dbConnection;
    FloatingActionButton fab;
    Button btn_positive, btn_negative;
    EditText et_year;
    EditText et_month;
    Receipt rec;
    public Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbConnection = new DBConnection(this);
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("Calling Fab ICon ");
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertdialog_custom_view, null);

                // Specify alert dialog is not cancelable/not ignorable
                builder.setCancelable(true);

                // Set the custom layout as alert dialog view
                builder.setView(dialogView);

                // Get the custom alert dialog view widgets reference
                btn_positive = (Button) dialogView.findViewById(R.id.dialog_positive_btn);
                btn_negative = (Button) dialogView.findViewById(R.id.dialog_negative_btn);
                et_year = (EditText) dialogView.findViewById(R.id.et_year);
                et_month = (EditText) dialogView.findViewById(R.id.et_month);

                // Create the alert dialog
                final AlertDialog dialog = builder.create();

                btn_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Dismiss the alert dialog
                        String year = et_year.getText().toString();
                        String month = et_month.getText().toString();
                        rec = new Receipt();
                        rec.set_year(year);
                        rec.set_month(month);
                        dialog.cancel();
                        dbConnection.addReceipt(new Receipt(rec.get_year(), rec.get_month()));
                        recreate();

                        System.out.println(rec.get_year()+"List of Years"+ rec.get_month()+"List of Months");


                    }
                });

                btn_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                                /*dialog.dismiss();
                                viewAll();*/
                                Toast.makeText(getApplication(),
                                        "No button clicked", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show();
            }
        });


        List<Receipt> receipts = dbConnection.getAllReceipts();
        System.out.println(receipts + "allReceipt");

        List<String> years = dbConnection.getYear();
        System.out.println(years + "Years");

        final HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> re = new ArrayList<String>();

        System.out.println(years.size() + "----Years Size");

        for (int i = 0; i < years.size(); i++) {
            expandableListDetail.put(years.get(i), dbConnection.getMonth(years.get(i)));
        }

        expandableListView = (ExpandableListView) findViewById(R.id.lvExp);
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);


        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {


                Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " : "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }


    public void recreate()
    {
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        this.finish();
    }

}

